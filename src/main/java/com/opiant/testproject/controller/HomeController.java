package com.opiant.testproject.controller;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.opiant.testproject.model.ArithmeticExpression;
import com.opiant.testproject.service.ExpressionEvaluator;

@Controller
@RequestMapping(value="/")
public class HomeController {

	
	/*public ModelAndView test(HttpServletResponse response) throws IOException{
		return new ModelAndView("home");
	}*/
	
	@RequestMapping(method = RequestMethod.GET)
    public String viewHistory(Map<String, Object> model) {
        //User userForm = new User(); 
        ArithmeticExpression arithmeticExpresion = new ArithmeticExpression();
        arithmeticExpresion.setResult("1+2=3");
        model.put("arithmeticExpression", arithmeticExpresion);
         
        /*List<String> professionList = new ArrayList<>();
        professionList.add("Developer");
        professionList.add("Designer");
        professionList.add("IT Manager");
        model.put("professionList", professionList);*/
         
        return "home";
    }
     
    @RequestMapping(value="/calculate",method = RequestMethod.POST)
    public String processExpression(@ModelAttribute("arithmeticExpression") ArithmeticExpression expression,
            Map<String, Object> model) {
        
        int result = ExpressionEvaluator.evaluate(expression.getExpression());
        expression.setResult(expression.getExpression()+"="+result);
        // for testing purpose:
        System.out.println("Input: " + expression.getExpression());
        System.out.println("Result: " + expression.getResult());
         
        return "result";
    }
}
